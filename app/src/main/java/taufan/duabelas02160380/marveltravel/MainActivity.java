/*
  Dibuat oleh: Leonardo Taufan Sontani
  Kelas: SI-40-01
  NIM: 1202160380
  Aplikasi ini dibuat untuk memenuhi tugas praktikum PAB Modul 2,
  dan ditujukan untuk edukasi.
  Logo splash-screen didapatkan dari "Really Useful Dragons at Skyrim Nexus"
  https://www.nexusmods.com/skyrim/mods/48593/
  MainActivity.java
 */
package taufan.duabelas02160380.marveltravel;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity
        extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener, View.OnClickListener {
    private TextView saldoTextView, txtDate, txtTime;
    private int jumlahsaldo = -1;
    private int min_saldo = 100000;
    private int tujuan = 0;
    private Button btnDatePicker, btnTimePicker;
    private int mMinute, mHour, mDay, mMonth, mYear;
    private Switch switch_pp;
    private String date, item;
    private int day, month, years;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity-onCreate", "ART!!! Start...your...engine...!!!");
        setTheme(R.style.AppTheme); //untuk splash
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //nominal saldo
        saldoTextView = findViewById(R.id.jumlah_saldo);
        jumlahsaldo = Integer.parseInt(saldoTextView.getText().toString());
        if (jumlahsaldo < min_saldo) {
            saldoTextView.setTextColor(Color.RED);
        }
        //Spinner dan kawan-kawannya
        Spinner tujuannya = findViewById(R.id.tujuan_spinner);
        if (tujuannya != null) {
            tujuannya.setOnItemSelectedListener(this);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.tujuan_array, android.R.layout.simple_spinner_item);
        if (tujuannya != null) {
            tujuannya.setAdapter(adapter);
        }

        //Tanggal dan Waktu
        btnDatePicker = findViewById(R.id.tanggal_btn);
        btnTimePicker = findViewById(R.id.waktu_btn);
        txtDate = findViewById(R.id.date);
        txtTime = findViewById(R.id.time);
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        //Switch
        switch_pp = findViewById(R.id.pp_switch);
        Log.d("MainActivity-onCreate", "onCreate selesai, bosq");
    }

    public void topup(View view) {
        Log.d("MainActivity-topup", "topup...mulai!");
        LayoutInflater linf = LayoutInflater.from(this);
        final View inflator = linf.inflate(R.layout.dialog_topup, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Topup");
        alert.setMessage("Masukkan nominal saldo");
        alert.setView(inflator);

        final EditText et1 = inflator.findViewById(R.id.popup_topup);

        alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                String s1=et1.getText().toString();
                Log.d("Log", s1);
                jumlahsaldo = jumlahsaldo + Integer.parseInt(s1);
                saldoTextView.setText(Integer.toString(jumlahsaldo));
                if (jumlahsaldo > min_saldo) {
                    saldoTextView.setTextColor(Color.BLACK);
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });

        alert.show();
        Log.d("MainActivity-Topup", "Topup berhasil! Aaaah, pengen main Skyrim...");
    }


    public void beliTiket(View view) {
        Log.d("MainActivity-beliTiket", "beliTiket...mulai!");
        EditText jml = findViewById(R.id.jumlah_tiket);
        int jumlahtiket = Integer.parseInt(jml.getText().toString());
        int biayatravel = tujuan * jumlahtiket;
        if (switch_pp.isChecked()) {
            biayatravel = biayatravel * 2;
        }
        if (jumlahsaldo < biayatravel) {
            Toast.makeText(this, "Saldo tidak cukup!", Toast.LENGTH_LONG)
                    .show();
        } else {
            try {
                Intent summary = new Intent(this, Summary.class);
                Bundle bundle = new Bundle();
                bundle.putInt("tujuan", tujuan);
                bundle.putInt("jam", mHour);
                bundle.putString("tujuannya", item);
                bundle.putInt("menit", mMinute);
                bundle.putBoolean("switche", switch_pp.isChecked());
                if (switch_pp.isChecked()) {
                    bundle.putString("tanggalpulang", (day + 1) + "/" + (month) + "/" + years);
                }
                bundle.putInt("biaya", biayatravel);
                bundle.putInt("jmlTiket", jumlahtiket);
                bundle.putString("tanggal", date);
                Log.d("MainActivity-Bundle", "Bundle created!");
                summary.putExtras(bundle);
                Log.d("MainActivity-Bundle", "Bundle inserted!");
                startActivityForResult(summary, 1);
            } catch (Exception e) {
                Toast.makeText(this, "Error! "+e, Toast.LENGTH_LONG)
                    .show();
            }
        }
        Log.d("MainActivity-beliTiket", "beliTiket selesai! Semangat, ya, Gigi!");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("MainActivity-Result", "onActivityResult...mulai!");
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("newsaldo");
                jumlahsaldo = jumlahsaldo - Integer.parseInt(result);
                Log.d("onActivityResult", Integer.toString(jumlahsaldo));
                saldoTextView.setText(Integer.toString(jumlahsaldo));
                if (jumlahsaldo > min_saldo) {
                    saldoTextView.setTextColor(Color.BLACK);
                } else {
                    saldoTextView.setTextColor(Color.RED);
                }
            }

        }
        Log.d("MainActivity-Result", "Selesai. Gigi kok pulangnya cepet amat? Kamu dikasih undangan atau ngelayat?");
    }

    //Ini digunakan untuk spinner
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String spinnerCurrent = adapterView.getItemAtPosition(i).toString();
        item = adapterView.getItemAtPosition(i).toString();
         switch(i) {
             case 0:
                 tujuan = 85000;
                 Log.d("Spinner", Integer.toString(tujuan));
                 break;
             case 1:
                 tujuan = 150000;
                 Log.d("Spinner", Integer.toString(tujuan));
                 break;
             case 2:
                 tujuan = 70000;
                 Log.d("Spinner", Integer.toString(tujuan));
                 break;
             default:
                 Log.d("Spinner", "Tidak ada yang terpilih!");
         }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Log.d("MainActivity-spinner", "Tidak ada yang terpilih!");
        //Tidak berguna karena default tujuan sudah ditentukan, namun dibutuhkan AdapterView
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d("MainActivity-onDateSet", "Diperlukan untuk ada oleh DatePicker, namun belum tahu kegunaannya apa...");
    }

    @Override
    public void onClick(View v) {
        if (v == btnDatePicker) {

            // Mengambil tanggal
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            Log.d("MainActivity", date);
                            day = dayOfMonth;
                            month = monthOfYear+1;
                            years = year;
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Mengambil waktu saat ini:
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            //Meluncurkan dialog TimePicker
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTime.setText(hourOfDay + ":" + minute);
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
}

